---
Week: 10
tags:
  - programmering
  - powershell
  - bash
---

# Øvelser uge 10

## Øvelse 1 - Bash script til vm konfigurering

### Information

I stedet for manuelt at opdatere og installere applikationer på en server/maskine/vm kan det være en god ide at gøre dette fra et script.  
Det er anvendt i industrien og hører under begrebet "immutable infrastructure".  
Immutable infrastructure handler om at man aldrig opdaterer softwaren på maskinen manuelt, men blot starter en ny virtuel maskine som har et script der automatisk henter nyeste opdateringer og compiler samt installerer hvad end der skal bruges (ved opstart). Det giver den fordel at man ved f.eks. angreb, nemt blot slukker den gamle webserver og starter en ny.

Eksempler/løsninger kan findes her:

- Øvelse 3 [https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/vm_deploy_scripts](https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/vm_deploy_scripts)

### Instruktioner

1. Sammen ser vi denne video om mutable/immutable infrastructure [https://youtu.be/II4PFe9BbmE](https://youtu.be/II4PFe9BbmE)
2. På klassen diskuterer vi hvordan man bruger dette og hvorfor det kan være smart? Hvad er ulemperne? Hvordan kører man et script ved opstart af en linux?
3. Installer en virtuel maskine i vmware, brug xubuntu 20.04 LTS som image.
   - Xubuntu image download [https://xubuntu.org/download/](https://xubuntu.org/download/)
   - hjælp til at installere en ny vm [https://kb.vmware.com/s/article/1018415](https://kb.vmware.com/s/article/1018415)
4. På din ny installerede vm lav et bash script der ved boot.
   - Checker om maskinen har forbindelse til internet
   - Automatisk opdaterer maskinen (via apt-get update/upgrade).
   - Installerer apache2 og starter webserveren (check at den kører på localhost port 80)
   - Logger installations output i en text fil (her kan [https://linuxize.com/post/linux-tee-command/](https://linuxize.com/post/linux-tee-command/) være en god kandidat)
   - EKSTRA: Få scriptet til at checke om der findes nye opdateringer og kun kører hvis der er.
   - EKSTRA: Udvid scriptet til at acceptere en liste af applikationer som scriptet installerer.
5. Lav en klon af din xubuntu vm og check at scriptet samt apache også kører ved boot på den klonede vm

## Øvelse 2 - Powershell basics

### Information

For at lære powershell at kende er her en Microsoft learning path som du kan gennemføre. Du skal oprette en azure konto for at bruge deres virtuelle maskiner. Alternativt kan du bruge f.eks. Kali Linux.  
Når du har gennemført og samtidig har kendskab til bash og python bør du være i stand til at skrive små powershell scripts ved at bruge yderligere dokumentation som f.eks den officielle dokumentation fra Microsoft.

### Instruktioner

1. Lav så meget som muligt af _Automate administrative tasks by using PowerShell_ [https://docs.microsoft.com/en-us/learn/paths/powershell/](https://docs.microsoft.com/en-us/learn/paths/powershell/)
2. Dan dig et overblik over hvad du kan finde i den officielle dokumentation [https://docs.microsoft.com/en-us/powershell/scripting/how-to-use-docs?view=powershell-7.2](https://docs.microsoft.com/en-us/powershell/scripting/how-to-use-docs?view=powershell-7.2)

## Øvelse 3 - Powershell script

### Information

I denne øvelse skal vi sammen på klassen skrive nmapscan scriptet fra sidste uge, dene gang i powershell.  
Formen er at vi sammen omskriver og finder metoder til at skrive et script med samme funktionalitet.

### Instruktioner

1. Installer nmap i windows, kan findes her [https://nmap.org/](https://nmap.org/)
2. Omskriv nmapscan scriptet fra sidste uge til pseudo kode på klassens whiteboard.
3. Jeres underviser er hænder der skriver scriptet i powershell baseret på jeres instruktioner.
