---
Week: 09
tags:
  - programmering
  - netværk
  - python
  - bash
---

# Øvelser uge 9

Del klassen i 2 grupper, hver gruppe laver øvelser i fællesskab. Formålet er at i kan hjælpe hinanden med at komme i mål.

Eksempler/løsninger kan findes her:

- Øvelse 2 [https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/nmapscan_scripts](https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/nmapscan_scripts)
- Øvelse 3 [https://gitlab.com/npes-py-experiments/network-programs](https://gitlab.com/npes-py-experiments/network-programs)

## Øvelse 1 - bash basics

### Information

Denne øvelse introducerer dig til bash og scripting i bash.  
Det er en forudsætning at du har adgang til en virtuel linux maskine. Gerne en kali linux kørende i f.eks vmware workstation pro.

En detaljeret gennemgang af opsætning og installation af kali linux på vmware er her: [https://www.kali.org/docs/virtualization/install-vmware-guest-vm/](https://www.kali.org/docs/virtualization/install-vmware-guest-vm/)

Hvis du vil køre kali på en anden host kan du se tilgængelige images her: [https://www.kali.org/get-kali/](https://www.kali.org/get-kali/)

### Instruktioner

1. Aftal i gruppen hvornår i skal være færdige med denne øvelse, i må ikke gå videre til næste øvelse før alle er færdige med denne øvelse.
2. Se denne korte video for at få et meget hurtigt indblik i bash [https://youtu.be/I4EWvMFj37g](https://youtu.be/I4EWvMFj37g)
3. Følg denne guide for at lære det fundamentale ved bash og bash scripting [https://linuxconfig.org/bash-scripting-tutorial-for-beginners](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)

## Øvelse 2 - Forstå bash script + tilrette script

### Information

Det er en forudsætning for denne øvelse at du har en virtuel linux maskine (Linux VM) med nmap installeret.

Nmap kan findes her: [https://nmap.org/](https://nmap.org/)
og installeres ved først at opdatere apt package list med `sudo apt-get update` og derefter installere nmap med `sudo apt-get install nmap`

Check om nmap er installeret med `nmap --version`

### Instruktioner

1. Recap hvad i har lært i øvelse 1 ved at kigge på dette cheat sheet [https://devhints.io/bash](https://devhints.io/bash)
2. Diskuter nedenstående script i gruppen. Hvad sker der på hver linje i dette script? – I skal snakke det igennem FØR I kører koden. Ideen er at forstå koden.
3. Implementer dette script i din linux VM. Når I har fået det til at virke, så tilret således nmap resultatet ikke skrives til en fil, men at alle de åbne porte printes direkte i konsolvinduet (uden anden output).

![script](nmapscans_example.png)

## Øvelse 3 - Programmering med netværk

### Information

Python har flere biblioteker der gør det forholdsvist simpelt at kommunikere over netværk.  
I denne øvelse får du fingrene i

- `socket` biblioteket til at kommunikere via sockets.
- `urllib` biblioteket der gør det simpelt at arbejde med _Uniform Resource Locators_ [https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL)
- `BeatifulSoup` til at parse html sider og filtrere på html tags, id, klasser mm.
- `Feedparser` til at parse RSS feeds [http://www.rss-specifications.com/](http://www.rss-specifications.com/)

I nogle af deløvelserne skal du bruge Wireshark, du er selvfølgelig velkommen til også at wireshark i de øvrige øvelser.

### Instruktioner

1. Fork projektet [https://gitlab.com/npes-py-experiments/network-programs](https://gitlab.com/npes-py-experiments/network-programs) til dit eget gitlab namespace
2. Klon projektet til din computer
3. Lav et virtual environment og installer dependencies som beskrevet i readme.md filen [https://gitlab.com/npes-py-experiments/network-programs#development-usage](https://gitlab.com/npes-py-experiments/network-programs#development-usage)

**socket**

1. Læs om pythons socket bibliotek [https://docs.python.org/3/library/socket.html](https://docs.python.org/3/library/socket.html)
2. Læs koden i `socket_1.py` Hvad gør koden og hvordan bruger den socket biblioteket?
3. Kør koden med default url og observer hvad der sker, du kan også prve at single steppe filen for at kunne følge med i variabler osv. [https://python.land/creating-python-programs/python-in-vscode](https://python.land/creating-python-programs/python-in-vscode)
4. Åben wireshark (installer det hvis du ikke allerede har det)
5. Kør koden igen med default url, analyser med wireshark og besvar følgende:
   - Hvad er destinations ip ?
   - Hvilke protokoller benyttes ?
   - Hvilken content-type bruges i http header ?
   - Er data krypteret ?
   - Hvilken iso standard handler dokumentet om ?
6. Læs `socket_2.py` og undersøg hvordan linje 38 - 47 kan tælle antal modtagne karakterer
7. Kør `socket_2.py` og prøv at hente forskellige url's der benytter https, hvad modtager du, hvilken http response får du, hvad betyder de og er de krypteret ?
8. Gentag trin 7 og analyser med wireshark for at finde de samme informationer der
9. Åben `socket_3.py`, analyser koden og omskriv den så kun http headers udskrives.

**urllib**

1.  Læs om urllib biblioteket [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html)
2.  Åbn `urllib_1.py` og læs koden, hvordan er syntaksen ifht. socket 1-3 programmerne ?
3.  Hvilket datasvar får du retur hvis du prøver at hente via https, f.eks [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html)
4.  Kør programmet igen med [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html) og analyser i wireshark, besvar følgende:
    - Hvad er destinations ip ?
    - Hvilke protokoller benyttes ?
    - Hvilken content-type bruges i http(s) header ?
    - Er data krypteret ?
    - Hvor mange cipher suites tilbydes i `Client hello` ?
    - Hvilken TLS version og cipher suite bliver valgt i `Server Hello` ?

**BeautifulSoup**

1. Læs om BeatifulSoup biblioteket [https://beautiful-soup-4.readthedocs.io/en/latest/](https://beautiful-soup-4.readthedocs.io/en/latest/)
   - Hvad er formået med biblioteket ?
2. Åbn `urllib_2.py` og analyser koden for at finde ud af hvad den gør.
3. Kør programmet
4. Ret i programmet så det tæller et andet HTML tag
5. Ret i programmet så det bruger BeatifulSoups `.findall()` metode

**Feedparser**

1. Læs om feedparser biblioteket [https://feedparser.readthedocs.io/en/latest/](https://feedparser.readthedocs.io/en/latest/)
2. Åbn `rssfeed_parse.py` og analyser koden for at finde ud af hvad den gør.
3. Linje 28, som formatterer modtaget data, har en meget lang sætning.

   ```python
   {re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ``
   ```

   Hvad gør den ? ( du kan prøve at erstatte den med `{entry.summary}` for at se forskellen)

4. Vælg et feed du vil parse fra [https://blog.feedspot.com/cyber_security_rss_feeds/](https://blog.feedspot.com/cyber_security_rss_feeds/)
5. Omskriv `rssfeed_parse.py` til at bruge dit valgte feed, ret i formatteringen, så du får et output i markdown filen, der svarer til:

   ```markdown
   # Headline/title

   **_Author:_**
   **_Summary:_**
   [link_to_full_article](link_to_full_article)
   ```

6. Tænk over om den genererede markdown fil på en eller anden måde vil kunne bruges på jeres fælles **ucl-itsec-student-ressources** side ? (kan et python script køres som en del af gitlabs CI/CD pipeline ?)

### Links

[https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)
