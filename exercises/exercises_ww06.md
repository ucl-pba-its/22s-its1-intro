---
Week: 06
tags:
  - Introduktion
  - Overblik
  - Forventningsafstemning
  - Kompentenceafklaring
  - Nyhedskanaler
  - Karriere
  - Præsentation
---

# Øvelser uge 6

## Øvelse 1 - Kompetence afklaring

### Information

I har alle forskellige baggrunde. For at lære jeres forudsætninger, i forhold til fagets læringsmål, at kende vil jeg gerne vide hvordan i subjektivt vurderer jeres faglige evner.
Til det har jeg lavet et spørgeskema som i alle skal udfylde.

**Spørgeskemaet kan kun tilgås ved at bruge din UCL konto's user og pass**

_20 minutter_

### Instruktioner

1. Udfyld spørgeskemaet [https://forms.gle/2eqoSf9qANhf7DRP8](https://forms.gle/2eqoSf9qANhf7DRP8)

## Øvelse 2 - Markdown

### Information

Det i læser lige nu er skrevet som tekstfil og derefter renderet til html og pdf ved hjælp af en CI/CD pipeline på Gitlab.  
Tekstfilens format er Markdown. Markdown har en meget simpel formaterings syntax og renderes default på mange platforme som f.eks github, udover det bruges Markdown bruges mange andre steder.

Endelig er markdown, efter lidt tilvænning, virkelig brugbart til at tage noter, både på studiet eller når i laver pentesting etc.

I næste uge skal vi igang med Gitlab hvor markdown også er understøttet, derfor vil jeg gerne have at i kender markdown og begynder at bruge det.

_45 minutter_

### Instruktioner

1. Læs om markdown og hvad du kan bruge det til [https://www.markdownguide.org/getting-started](https://www.markdownguide.org/getting-started)
2. Kig på markdown syntax i dette cheatsheet [https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)
3. Undersøg hvordan du kan previewe markdown i din favorit editor.
4. Lav en test markdown fil `.md` og prøv mulighederne af ved hjælp af cheat sheetet fra punkt 2. (det kan godt være du skal søge lidt ekstra på nettet for at gøre alt nedenstående)
   - Lav 3 forskellige overskrifter
   - Lav et link
   - Indsæt et billede (link til et billede)
   - Lav en 4\*4 tabel med første kolonne venstre justeret, kolonne 2 og 3 centreret og kolonne 4 højre justeret.
   - Lav en unordered list (bullet points) med 3 niveauer
   - Lav en ordered list med 2 niveauer
   - Lav fed tekst og italic tekst. Kan man lave fed italic tekst samtidig ?
   - Lav en code block og angiv det sprog koden er skrevet i.
   - Find selv på mere hvis du har ekstra tid. Kig evt. på bonus herunder.

_Bonus_ [https://obsidian.md/](https://obsidian.md/) er ret kool til at holde styr på dine markdown filer lokalt, du kan selvfølgelig gemme din vault remote hvor du ønsker det. Hvem ved, måske bliver det måden du laver notater igennem uddannelsen ?  
[https://joplinapp.org/](https://joplinapp.org/) er også en mulighed som er lidt mindre manuel i forhold til obsidian.

## Øvelse 3 - Studieordning og læringsmål

_Individuel opgave: tid er angivet i opgavens instruktioner_

### Information

For at være sikker på at du kender uddannelsens grundlag, læringsmål, eksamener etc. skal du kende indholdet i de mest vigtige dokumenter der hører til uddannelsen.
Du kan finde uddannelsens dokumenter på [https://www.ucl.dk/uddannelser/it-sikkerhed#uddannelsens+opbygning](https://www.ucl.dk/uddannelser/it-sikkerhed#uddannelsens+opbygning)  
Nogen af dokumenterne kan findes ved at klikke på "Øvrige studiedokumenter"

### Instruktioner

1. Dan dig et overblik over hvad der står i "Studieordning National", hvad indeholder den og hvad står under de forskellige sektioner. Noter de spørgsmål du har til den nationale studieordning. _20 minutter_
2. Dan dig et overblik over hvad der står i "Studieordning Institutionel del 2022 (først gældende fra 1.feb)", hvad indeholder den og hvad står under de forskellige sektioner. Noter de spørgsmål du har til den Institutionel del af studieordningen. _20 minutter_
3. Læs læringsmålene for "Introduktion til IT Sikkerhed" i studieordningen - Noter de spørgsmål du har til læringsmålene. _15 minutter_
4. Læs om "Prøve i Introduktion til IT-sikkerhed", Noter spørgsmål til indholdet. _15 minutter_
5. Opsamling på klassen baseret på jeres spørgsmål. _15-30 minutter_

## Øvelse 4 - Nyhedskanaler

### Information

Næsten dagligt er der nyheder om it sikkerheds branchen. Det kan være nyheder om nye botnet, 0day, sikkerheds forskning etc.

Men hvor kan disse nyheder findes og hvad er den bedste måde at modtage dem ?

Formålet med denne øvelse er at få jer til at følge med i branchens nyheder. Vi kommer til at starte hver undervisningsgang i faget med at snakke om hvad der rører sig i it sikkerheds branchen, selvfølgelig baseret på hvad i synes der er relevant ud fra de nyheder i læser igennem ugen.

_45 minutter_

### Instruktioner

1. Research kilder til nyheder om it sikkerhed. Lav en liste der for hver kilde indeholder:
   - Web adresse
   - Type af nyheder
   - Hvem står bag siden ?
   - Hvordan kan du tilgå nyhederne (RSS, mail, api, manuelt besøg, andet?)
2. Gå sammen med dine medstuderende og lav et samlet markdown dokument der har ovenstående oplysninger. Hvordan kan i dele dokumentet så alle har adgang?
3. Bliv enige om hvordan og hvor ofte det er mest hensigtsmæssigt at tilgå nyhederne, er det muligt f.eks at lave en applikation der scraper nyheder fra alle jeres kilder ?

## Øvelse 5 - Jobmuligheder

### Information

I sidste uge havde vi en kort snak om hvilke muligheder i har for at få praktik plads og job. Vi snakkede også om hvad i helst vil arbejde med når i er færdige med uddannelsen.  
Denne øvelse er ment som en hjælp til at få et overblik over hvilke titler der er relevante for jer at søge beskæftigelse indenfor.  
Idéen er også at i får oprettet en søge agent på en eller flere job databaser så i løbende holder øje med praktik og karriere muligheder.

_45 minutter_

### Instruktioner

1. Undersøg hvilke jobdatabaser der er relevante for it området. Som minimum synes jeg i skal bruge [https://www.jobindex.dk](https://www.jobindex.dk) og [https://www.it-jobbank.dk](https://www.it-jobbank.dk)
2. Lav søgninger i de jobdatabaser i har fundet i punkt 1. Find frem til de søgeord der giver flest relevante resultater.  
   Ved at søge samlet på `it-sikkerhed`, `informationssikkerhed` og `cybersecurity` fandt jeg på jobindex 146 stillinger.  
   På it-jobbank fandt jeg 44 stillinger.
3. Opret en job/søgeagent på dine valgte jobdatabaser så du løbende modtager information om stillinger og jobmuligheder.  
   Husk at søge bredt, både geografisk og i forhold til brancher, gå efter den søgning der giver dig flest resultater. Formålet er at få overblik, senere på uddannelsen ,når du ved hvad du skal søge efter, kan du lave mere præcise søgeagenter.
4. Lav et samlet og delt markdown dokument hvor i lister jeres jobdatabaser og søgekriterier. Del dokumentet på klassen og med mig.
5. Som afslutning på øvelsen tager vi en runde på klassen hvor i præsenterer hvilke jobdatabaser i har fundet og hvilke søgekriterier i har udvalgt.

## Øvelse 6 - Præsentationsplan

### Information

At kunne kommunikere skriftligt og mundtligt er en stor del af at arbejde med it sikkerhed. Du skal kunne kommunikere din viden og ekspertise og sætte den i kontekst for andre, der ikke har samme viden og ekspertise.  
For eksempel har ledelse et andet fokus, ofte økonomi, så for at få dem til at forstå f.eks. investeringer i sikkerheds tiltag, skal du kunne formidle din viden på en måde der gør din leder i stand til at træffe de rette beslutninger.
Nysgerrighed og research er en anden vigtig kompetence. Du er nok allerede vant til at skulle sætte dig ind i nye teknologier eller værktøjer.  
Denne øvelse er ikke anderledes, du vil få trænet din evne til at researche et emne og samtidig overveje hvordan du kan formidle din viden til andre.

Endelig afsluttes de fleste fag med en mundtlig eksamen, også i denne kontekst er ovenstående vigtigt at træne.

Alle studerende skal i løbet af semestret præsentere et emne der er indeholdt i faget "introduktion til it sikkerhed", det er ok hvis i går to sammen om en præsentation, kravet er at i alle skal præsentere mindst en gang i løbet af semestret.

Der er afsat 30 minutter til hver præsentation inkl. spørgsmål.

Emnerne er:

- Programmering i Python med sockets
- Programmering i Python med databaser
- Bash Scripting
- Powershell scripting
- Netværk - osi model, wireshark samt vigtigste netværksprotokoller og deres sikkerhedsniveau
- Kali linux (f.eks. nmap og metasploit)

_60 minutter_

### Instruktioner

1. Gå sammen 2 og 2 og snak om hvad der er vigtigt i en præsentation ud fra nedenstående spørgmål.
   - Hvem er dit publikum ? Hvad ved de i forvejen og hvad optager dem ?
   - Hvordan laver du overblik over det du vil præsentere og hvordan kan du skabe flow i din præsentation ?
   - Hvor meget information skal du have på hver af dine slides ? Hvad er essensen af det du præsenterer på det enkelte slide ?
   - Vil du involvere dit publikum og få dem til at udføre ting i løbet af din præsentation ? Vil du demonstrere noget live ? Hvordan ? Hvordan styrer du tid ?
   - Hvad med referencer og kilder til det du præsenterer ? Hvordan kan du dele det med dit publikum så de selv kan finde mere information ?
   - Hvordan agere du selv under præsentationen ? Hvordan skaber du kontakt til og engagement med dit publikum ?
2. Skriv essensen af jeres overvejelser ned
3. Gå til uge 6 planen på itslearning og skriv dit navn på et af emnerne i "rækkefølge oplæg" linket.
4. Øvelsen afsluttes på klassen med en kort runde af hvad i har valgt at præsentere og hvilke overvejelser i har gjort omkring arbejdet med præsentationen. Det gør vi i uge 7.
