---
Week: 14
tags:
  - Repetition
  - Eksamens forberedelse
---

# Øvelser uge 14 - Forberedelse til eksamen

## Øvelse 1 - Overblik over fagets øvelser og læringsmål

### Information

I har igennem semestret arbejdet med fagets læringsmål igennem forskellige øvelser.  
I denne opgave skal i danne jer et overblik over hvad i har arbejdet med og hvor i har brug for at repetere op til eksamen.

### Instruktioner

1. Lav et dokument med en overskrift for hver øvelse beskrevet i fagets materiale [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/)
2. Gennemse fagets læringsmål på itslearning
3. For hver øvelse noter hvad du mangler i forhold til læringsmål.

## Øvelse 2 - Eksamens spørgsmål

### Information

Til eksamen bør du forberede en præsentation for hvert spørgsmål du kan trække.  
Formålet med denne øvelse er at få et overblik over eksamens spørgsmålene og hvad du kan læse op på inden eksamen.

### Instruktioner

1. Læs eksamens opgaven [https://ucl-pba-its.gitlab.io/22s-its1-intro/22S_ITS1_INTRO_eksamens_opgave.pdf](https://ucl-pba-its.gitlab.io/22s-its1-intro/22S_ITS1_INTRO_eksamens_opgave.pdf)
2. Sammenhold eksamens spørgsmålene med dit dokument fra øvelse 1 og noter hvad du bør læse op på for at kunne besvare spørgsmålene.
3. Lav en plan for hvordan du vil læse op og forberede præsentationer.
4. Noter spørgsmål som vi skal gennemgå på klassen i dagens lektioner.

## Øvelse 3 - Repetition på klassen

### Information

Vi har denne ene undervisningsgang før eksamen, derfor er det vigtigt at vi får repeteret fagligt stof som du har identificeret i øvelse 1 og 2.

### Instruktioner

1. På klassen laver vi en liste over emner som skal gennemgås. Vi kan ikke nå at gennemgå alt fra semestret så vurder hvad der er det vigtigeste for dig.

2. Vi gennemgår undervisnings materiale fra tidligere uger, baseret på listen fra punkt 1.
