---
Week: 13
tags:
  - vmware
  - netværk
  - juniper
  - junos
  - kali linux
---

# Øvelser uge 13

## Øvelse 1 - Fortsæt øvelser fra uge 12

### Information

Arbejd videre med øvelserne fra uge 12, fortsæt hvor du nåede til i sidste uge.  
Målet er at du når til og med øvelse 9 - Damn Vulnerable Web Application (DVWA)

### Instruktioner

- Se [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12)

### Links

Links hvis nødvendigt
