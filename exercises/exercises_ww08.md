---
Week: 08
tags:
  - programmering
  - database
  - python
---

# Øvelser uge 8

Eksempler/løsninger kan findes her:

- Øvelse 2 [https://gitlab.com/npes-py-experiments/database-programs](https://gitlab.com/npes-py-experiments/database-programs)

## Øvelse 1 - Python recap

### Information

For lige at komme op i gear tager denne øvelse dig igennem de grundlæggende ting i Python.  
Hvis du ikke har programmeret i Python før burde det være muligt at overføre din viden fra andre programmeringssprog.

Øvelsen er individuel og du skal bruge nedenstående links som reference når du skal programmere de forskellige programmer.

- [https://www.w3schools.com/python/default.asp](https://www.w3schools.com/python/default.asp)
- [https://docs.python.org/3/reference/index.html](https://docs.python.org/3/reference/index.html)

Det kan ofte være en hjælp at skrive lidt pseudo kode eller lave et flow diagram inden du starter med at kode.

### Instruktioner

1. Vi starter sammen med en lille Python quiz, den er her: [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp)
2. Lav et gitlab repo, klon det til din computer, lav et virtuel environment og start det.
3. Lav en .gitignore fil og ekskluder env mappen
4. Lav et python program der bruger følgende datatyper:
   - string, int, float, list, tuple, dict, set og bool.  
     Det behøver ikke være et meningsfuldt program, bare du får prøvet datatyperne. Du kan printe output til cli med `print()`.
5. Lav et python program der bruger følgende flowstrukturer:
   - for loop, while loop, if/else, try/except, input.
     Det behøver ikke være et meningsfuldt program, bare du får prøvet strukturene. Du kan bare printe output til cli.
6. Lav et nyt python program, en basis lommeregner som kan `+ - * /`  
   Programmet skal kunne tage input fra bruger og skrive resultater i cli. Du må kun bruge funktioner til de fire regneoperationer.
7. Udvid din lommeregner til at logge input og resultater til en .txt fil, lav en funktion der spørger bruger efter et filnavn der skal gemmes til og lav også en funktion som du kan kalde hver gang et input eller resultat skal gemmes. Filen må ikke overskrives mens programmet kører.
8. Omskriv dit regne program så regne funktionerne er i en klasse for sig selv og fil funktionerne er i en anden klasse. Brug klasserne så du får samme funktionalitet som i punkt 5.

Husk løbende at committe og selvfølgelig at pushe når det giver mening.

## Øvelse 2 - Programmering med database

### Information

I denne øvelse skal du fortsætte med python og lave et lille program der kan interagere med en relationel sqlite3 database ved hjælp af CRUD og SQL.  
Python bruger sqlite3 biblioteket til at interagere med sqlite databaser.

Hvis du behov for at åbne din database i et gui program er der et her [https://sqlitebrowser.org/](https://sqlitebrowser.org/)

Gem dine programmer på gitlab mens du arbejder.

### Instruktioner

1. Recap SQL syntax ved at prøve denne quiz [https://www.w3schools.com/sql/sql_quiz.asp](https://www.w3schools.com/sql/sql_quiz.asp)
2. Læs om databaser og sql i dette kapitel [https://www.py4e.com/html3/15-database](https://www.py4e.com/html3/15-database)
3. Læs om sqlite biblioteket [https://docs.python.org/3/library/sqlite3.html](https://docs.python.org/3/library/sqlite3.html)
4. Brug dette eksempel som udgangspunkt for dit første database program. Analyser eksemplet inden du går til næste punkt.

   ```python
   import sqlite3
   from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
   files_path = Path(str(Path.cwd()) + '/database-programs/databases/')
   print(files_path)

   # Create and connect to database
   conn = sqlite3.connect(files_path / 'music.db')

   cur = conn.cursor()

   # Create table in database
   with conn:
       cur.execute('DROP TABLE IF EXISTS Tracks')
       cur.execute('CREATE TABLE Tracks (title TEXT, plays INTEGER)')

   # Insert rows in tracks table
   with conn:
       cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('Thunderstruck', 20))
       cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('My Way', 15))

   # info to user
   print('All rows in the Tracks table:')

   # select values from table
   cur.execute('SELECT title, plays FROM Tracks')

   # print out the values
   for row in cur:
       print(row)
   cur.execute('DELETE FROM Tracks WHERE plays < 100')

   # Close database connection
   conn.close()
   ```

5. Udvid programmet til at opdatere en af linjerne i tracks tabellen.
6. Udvid programmet til at lave en ny tabel med et valgfrit antal felter.
7. Indsæt 10 linjer i den nye tabel
8. Hent linjerne og udskriv dem i alfabetisk orden.
9. Slet en af linjerne

**Database queries - chinook.db**

1. Læs om hvordan _chinook_ databasen er struktureret [https://www.sqlitetutorial.net/sqlite-sample-database/](https://www.sqlitetutorial.net/sqlite-sample-database/)
2. Hent databasen og pak den ud så du har filen _chinook.db_
3. Hent database diagrammet [https://www.sqlitetutorial.net/wp-content/uploads/2018/03/sqlite-sample-database-diagram-color.pdf](https://www.sqlitetutorial.net/wp-content/uploads/2018/03/sqlite-sample-database-diagram-color.pdf)
4. Lav et nyt program i en python fil der kan åbne og bruge databasen.
5. Skriv en query der lister alle tabellerne alle tabellerne i _chinook.db_ ?
6. Skriv en query der viser kunder der ikke bor i US, vis _full name_ _customer ID_ _country_
7. Skriv en query der viser alle kunder fra _brazil_
8. Skriv en query der viser _invoices_ fra kunder i _brazil_ udskriv _full name_ _Invoice ID_ _Invoice date_ _billing country_
9. Skriv en query der viser alle kunder fra _brazil_
10. Hvis du har lyst til at prøve flere sql queries i chinook databasen kan du finde flere her [https://github.com/kenziebottoms/nss-back-02-chinook](https://github.com/kenziebottoms/nss-back-02-chinook)

_Hint: Du kan også skrive queries i sqlite browser som øvelse før du implementerer i dit python program_
