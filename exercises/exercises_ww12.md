---
Week: 12
tags:
  - vmware
  - netværk
  - juniper
  - junos
  - kali linux
---

# Øvelser uge 12

Øvelserne i denne uge bygger alle på følgende netværks diagram

![netværksdiagram](vrsx_intro_til_itsik.png)

Det overordnede mål med øvelserne er, udover at lære om netværk i praksis, at opsætte et lille virtuelt lab som kan bruges til pen testing med kali linux og et par maskiner + router

Her er et par ressourcer som jeg har brugt, mens jeg lavede ugens øvelser:

- Per Dahlstrøm networking pages [https://perper.gitlab.io/networkingpages/#](https://perper.gitlab.io/networkingpages/#)
- Junos cheat sheet [http://net.cmed.us/Home/juniper/junos-cheat-sheet](http://net.cmed.us/Home/juniper/junos-cheat-sheet)
- vmware virtual serial port [https://docs.vmware.com/en/VMware-Workstation-Pro/16.0/com.vmware.ws.using.doc/GUID-70C25BED-6791-4AF2-B530-8030E39ED749.html](https://docs.vmware.com/en/VMware-Workstation-Pro/16.0/com.vmware.ws.using.doc/GUID-70C25BED-6791-4AF2-B530-8030E39ED749.html)
- vsrx additional interfaces [https://www.juniper.net/documentation/us/en/software/vsrx/vsrx-consolidated-deployment-guide/vsrx-vmware/topics/task/security-vsrx-vmware-adding-interfaces-update.html#id-adding-vmxnet-3-interfaces](https://www.juniper.net/documentation/us/en/software/vsrx/vsrx-consolidated-deployment-guide/vsrx-vmware/topics/task/security-vsrx-vmware-adding-interfaces-update.html#id-adding-vmxnet-3-interfaces)
- vsrx set password [https://www.juniper.net/documentation/us/en/software/junos/user-access/topics/topic-map/user-access-root-password.html](https://www.juniper.net/documentation/us/en/software/junos/user-access/topics/topic-map/user-access-root-password.html)
- Damn Vulnerable Web Application github [https://github.com/digininja/DVWA](https://github.com/digininja/DVWA)
- Damn Vulnerable Web App Guide 2019 [https://github.com/mrudnitsky/dvwa-guide-2019](https://github.com/mrudnitsky/dvwa-guide-2019)
- Installing and Configuring DVWA [https://www.thomaslaurenson.com/blog/2018-07-12/installing-and-configuring-DVWA/](https://www.thomaslaurenson.com/blog/2018-07-12/installing-and-configuring-DVWA/)

## Øvelse 1 - Opsætning af virtuelle maskiner i vmware

### Information

I denne øvelse skal du opsætte 2 virtuelle maskiner i vmware. Maskinerne skal bruge xubuntu 18.04 LTS som image.  
Hvis du har brug for en genopfriskning af hvordan du installerer en xubuntu VM så kig på øvelserne fra uge 10.

### Instruktioner

1. Lav 2 xubuntu VM maskiner
2. Konfigurer de 2 linux maskiner's netværk ifølge netværks diagrammet
3. åbn vm indstillinger for hver xubuntu maskine. Sæt xubuntu 1 til vmnet1 og xubuntu 2 til vmnet2 (hvis du ikke har vmnet2 så lav det i vmware virtual network editor)

## Øvelse 2 - Opsætning af vsrx virtuel router

### Information

I denne øvelse skal du opsætte en virtuel juniper vsrx (juniper srx240)

### Instruktioner

1. download vm filer fra itslearning
2. åben ovf fil i vmware workstation
3. giv vsrx vm et navn
4. tryk import
5. åbn indstillinger og forbind ge-0/0/1 til vmnet1 og ge-0/0/2 til vmnet2 (hvis du ikke har vmnet2 så lav det )  
   adapter 1 er interface ge-0/0/0, adapter2 er ge-0/0/1, adapter3 er ge-0/0/2 (se netværks diagram)
6. konfigurer vmnet 1 og 2 i virtual network editor. Kun _host only_ skal være valgt. Klik apply og check settings.

### Links

Per fra IT-Teknolog har lavet en video om hvordan det gøres, det kan evt. være en hjælp [https://youtu.be/SlPj1QYHzlM](https://youtu.be/SlPj1QYHzlM)

## Øvelse 3 - Seriel forbindelse til vsrx virtuel router

### Information

I denne øvelse skal du bruge _putty_ og _windows named pipes_ til at forbinde til vsrx konsollen.  
Du kan godt bruge konsollen direkte i vmware men min oplevelse er at den er langsom, det er også lettere at kopiere tekst til/fra putty.

Hvis du er på linux eller mac så vil det her ikke virke da _named pipes_ er en windows ting. Jeg kom så langt som at finde noget i osx der hedder _mkfifo_ men jeg har ikke en mac og kan ikke teste det.  
Hvis du er på mac, kan du måske bruge tiden på at finde en løsning til mac ?

### Instruktioner

1. Hent og installer putty hvs du ikke allerede har det på din computer [https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
2. tilføj en seriel port til vsrx i vm settings
3. rediger seriel porten til _use named pipe_ og brug syntax `\\.\pipe\<pipenavn>`
4. åben putty og konfigurer en seriel forbindelse (serial line) med pipe navnet, baudrate 9600, 8 databits, 1 stopbit, parity + flowcontrol none
5. gem putty konfigurationen
6. boot vsrx
7. forbind med putty og konfirmer at du får output fra din vsrx vm
8. default user er `root` og intet password (det konfigureres i næste øvelse)

### Links

Igen har Per fra IT-Teknolog lavet en video om hvordan det gøres [https://youtu.be/ft5UBqMAlRw](https://youtu.be/ft5UBqMAlRw)

## Øvelse 4 - konfigurer vsrx password og vsrx navn

### Information

Af åbenlyse grunde er det god praksis, som minimum at give din router et password, det er også god praksis at navngive routere.  
Det skal du gøre i denne øvelse.
Juniper vsrx er baseret på linux og det er faktisk linux du først møder når du logger på med root. For at konfigurere routeren skal du logge på junos som er junipers styresystem. Det gøres med kommandoen `cli`

### Instruktioner

1. login på vsrx med root og derefter start til junos med `cli`
2. skriv `edit` for at komme i konfiguration mode
3. skriv `edit system root-authentication` (tab completion kan bruges til kommandoer)
4. skriv `set plain-text-password` dit password skal være minimum 6 karakterer og må ikke kun være tal, jeg mener også der skal være et stort bogstav. Du vil se en fejlmeddelse hvis du ikke overholder password politikken. Gentag indtil dit password er accepteret.
5. skriv `commit` for at gemme ændringer, vent på _commit complete_ i terminalen
6. skriv `exit` flere gange for at komme til login igen
7. login med root og dit nye password (**husk at skrive det ned til senere brug!**)
8. for at give routeren et navn, gå i edit mode igen
9. skriv `set system host-name <navn>` og derefter `commit`.
10. Kontroller navnet ved at skrive `show system host-name`

## Øvelse 5 - konfigurer vsrx routing

### Information

Nu er det tid til at konfigurere vsrx til routing mellem de 2 xubuntu maskiner, husk at kigge på netværksdiagrammet når du arbejder.

### Instruktioner

1. i edit mode skriv `edit interfaces`
2. konfigurer ge-0/0//1 til at bruge 192.168.10.0/24 netværket med kommandoen  
   `set ge-0/0/1 unit 0 family inet address 192.168.10.1/24`
3. konfigurer ge-0/0//2 til at bruge 192.168.11.0/24 netværket med kommandoen `set ge-0/0/2 unit 0 family inet address 192.168.11.1/24`
4. gem ændringer med `commit`
5. check routing table med `run show route terse`, output skal se ud som:

   ```
   inet.0: 4 destinations, 4 routes (4 active, 0 holddown, 0 hidden)
   + = Active Route, - = Last Active, * = Both

   A Destination        P Prf   Metric 1   Metric 2  Next hop         AS path
   * 192.168.10.0/24    D   0                       >ge-0/0/1.0
   * 192.168.10.1/32    L   0                        Local
   * 192.168.11.0/24    D   0                       >ge-0/0/2.0
   * 192.168.11.1/32    L   0                        Local
   ```

6. skriv `top` for at komme ud af edit interfaces
7. skriv `edit security zones` for at configurere de 2 interfaces så de begge er i trust zonen
8. inkluder ge-0/0/1 i trust zonen med `set security-zone trust interfaces ge-0/0/1 host-inbound-traffic system-services ping`
9. inkluder ge-0/0/2 i trust zonen med `set security-zone trust interfaces ge-0/0/2 host-inbound-traffic system-services ping`
10. fjern untrust zonen `delete security-zone untrust`
11. genetabler untrust til default med `set security-zone untrust`
12. gem ændringer med `commit`
13. skriv `show` for at se security zones konfigurationen
14. kontroller at dit ntævrk virker ved at pinge fra den ene computer til den anden, ping også de 2 router interfaces fra begge computere.

## Øvelse 6 - tilføj interface til vsrx

### Information

Udover de 2 xubuntu maskiner vil vi gerne have en kali linux VM på netværket også. Det betyder at vi mangler et interface på vsrx.  
Det er heldigvis simpelt at tilføje et nyt interface i vmware wworkstation, konfiguration af interfacet kan du jo allerede ;-)

### Instruktioner

1. sluk vsrx (power off vm)
2. åbn settings og tilføj en netværksadapter
3. åbn indstillinger og forbind network adapter 4 (ge-0/0/3) til vmnet3 (hvis du ikke har vmnet3 så lav det og konfigurer det)
4. boot router, log ind, start junos cli
5. skriv `show interfaces terse` for at se at du nu har ge-0/0/3
6. gå i edit mode `edit`
7. `edit interfaces`
8. konfigurer ge-0/0/3 med `set ge-0/0/3 unit 0 family inet address 192.168.12.1/24`
9. gem ændringer med `commit`
10. `show route terse` for at se routing tabel
11. `top` for at komme ud af edit interfaces
12. `edit security zones`
13. inkluder ge-0/0/3 i trust zonen med `set security-zone trust interfaces ge-0/0/3 host-inbound-traffic system-services ping`
14. gem ændringer `commit`
15. `show` for at se security zones config
16. ping router interfacet fra en af dine xubuntu maskiner for at bekræfte at dit netværk virker

## Øvelse 7 - Kali linux vm på netværket

### Information

På dit virtuelle netværk skal du nu opsætte en kali linux maskine der kan bruges som _attack box_

### Instruktioner

1. Installer en kali linux vm, brug den du har i forvejen, eller klon din eksisterende (vælg fuld klon)
2. sæt kali maskinens VM network adaptor til vmnet3
3. sæt maskinens ip til `192.168.12.2` netmask `255.255.255.0` gateway `192.168.12.1`
4. ping gateway og de 2 xubuntu maskiner for at bekræfte at dit netværk virker

## Øvelse 8 - Damn Vulnerable Web Application (DVWA)

### Information

Damn Vulnerable Web Application (DVWA) er en web applikation der med vilje er lavet med en masse usikkerheder indbygget. Formålet er at du kan øve web sikkerhed på applikationen.  
Øvelsen her har mange timer i sig og jeg forventer ikke at du laver den i uge 12.
I uge 13 er der ikke undervisning i "introduktion til IT sikkerhed" og det er meningen at du skal arbejde med kali og Damn Vulnerable Web Application (DVWA) i uge 13. Målet er at dykke ned i kali værktøjerne og udforske dem på egne hånd, nu har du jo dit eget lille lab til at gøre det :-)

Den officielle dvwa side er [https://dvwa.co.uk/](https://dvwa.co.uk/) med filerne hentes fra den tilhørende github side.

### Instruktioner

1. Rediger xubuntu1 til midlertidigt at bruge nat i vm settings, fjern også ip4 adressen i xubuntus netværksindstillinger og sæt forbindelsen til `dhcp` i stedet for `manual`.  
   Du skal muligvis disable og enable ethernet forbindelsen på maskinen. Ping 8.8.8.8 for at se om du har internet forbindelse
2. opdater apt listen med `sudo apt-get update`
3. Installer **Damn Vulnerable Web Application (DVWA)** på en af xubuntu maskinerne [https://github.com/digininja/DVWA](https://github.com/digininja/DVWA) ved at følge denne guide: [https://www.thomaslaurenson.com/blog/2018-07-12/installing-and-configuring-DVWA/](https://www.thomaslaurenson.com/blog/2018-07-12/installing-and-configuring-DVWA/)  
   Guiden er ikke helt ny og han skriver at DVWA bruger root som bruger i mysql databasen, det virkede ikke for mig fordi nyeste DVMA har en mysql bruger der hedder _dvma_. Jeg var derfor nød til at oprette en user der hedder dvma i mysql databasen, på samme måde som han forklarer omkring root user i guiden, med samme priviligier.
4. Når DVWA virker så ret xubuntu maskinens netværks konfiguration tilbage til tidligere konfiguration og sæt vm netværksinterfacet tilbage til vmnet.  
   Du skal muligvis disable og enable ethernet forbindelsen på maskinen. Ping noget på netværket for at se om din forbindelse virker. Ping 8.8.8.8 for at verificere at du **ikke** har internet adgang.
5. Gå til din kali maskine og naviger til DVMA (erstat 127.0.0.1 med dvma maskinens ip)
6. Kig på de forkellige øvelser på [https://github.com/mrudnitsky/dvwa-guide-2019](https://github.com/mrudnitsky/dvwa-guide-2019) og prøv et par stykker af dem :-)  
   Husk at skrive dine erfaringer ned når du laver øvelserne i dvwa, det hjælper ved fejlfinding og fordrer dybdelæring.
