---
title: '22S ITS1 Introduktion til IT Sikkerhed'
subtitle: 'Øvelser'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'Introduktion til IT Sikkerhed, øvelser'
---

# Introduktion

Dette dokument indeholder øvelser til faget, Introduktion til IT Sikkerhed, forår 2022.
Dokumentet er levende og opdateres løbende igennem semestret.

Dokumentet er et tillæg til fagets [ugeplaner](https://ucl-pba-its.gitlab.io/22s-its1-intro/22S_ITS1_INTRO_weekly_plans.pdf)
