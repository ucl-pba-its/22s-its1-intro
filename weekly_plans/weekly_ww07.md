---
Week: 07
Content: git, gitlab, ssh
Material: See links in weekly plan
Initials: NISI
---

# Uge 07 - git, gitlab, ssh

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Peronlig portfolio web side oprettet
- Fælles it-sikkerheds side oprettet

### Læringsmål

- Den studerende har viden om grundlæggende programerings principper
- Den studerende kan læse andres scripts samt gennemskue og ændre i dem

### Afleveringer

- Link til peronlig portfolio web side
- Link til fælles it-sikkerheds side

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet                                                      |
| :---: | :------------------------------------------------------------- |
| 9:00  | IT Sikkerheds nyheder                                          |
| 9:15  | Præsentationer fra studerende K2 - Mikkel (Python med sockets) |
| 9:45  | Introduktion til dagens øvelser K1                             |
| 10:00 | Øvelser K1/K4                                                  |
| 11:30 | Frokost                                                        |
| 12:15 | Øvelser K1/K4                                                  |
| 13:45 | Øvelser K2/K3                                                  |
| 16:15 | Fyraften                                                       |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww07](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww07)

## Kommentarer

- dr nyheder vestas hacket 2:10 - 12:45() [https://www.dr.dk/drtv/se/21-soendag\_-nicolai-mangler-hjaelp_298463](https://www.dr.dk/drtv/se/21-soendag_-nicolai-mangler-hjaelp_298463)
- dr nyheder læs [https://www.dr.dk/nyheder/viden/teknologi/frygtede-skulle-lukke-alle-vindmoeller-nu-aabner-vestas-op-om-hacking-angreb](https://www.dr.dk/nyheder/viden/teknologi/frygtede-skulle-lukke-alle-vindmoeller-nu-aabner-vestas-op-om-hacking-angreb)
