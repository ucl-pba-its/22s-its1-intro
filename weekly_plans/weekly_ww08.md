---
Week: 08
Content: Programmering i Python med database
Material: See links in weekly plan
Initials: NISI
---

# Uge 08 - Programmering i python med database

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Læringsmål opfyldt (øvelser lavet)

### Læringsmål

- Den studerende har viden om grundlæggende programmeringsprincipper
- Den studerende kan:
  - Anvende primitive datatyper og abstrakte datatyper
  - Konstruere simple programmer der bruge SQL databaser
  - Konstruere simple programmer der kan bruge netværk

## Afleveringer

- None

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet                          |
| :---: | :--------------------------------- |
| 9:00  | IT Sikkerheds nyheder              |
| 9:15  | Præsentationer fra studerende K2   |
| 9:45  | Introduktion til dagens øvelser K1 |
| 10:00 | Øvelser K1/K4                      |
| 11:30 | Frokost                            |
| 12:15 | Øvelser K1/K4                      |
| 13:45 | Øvelser K2/K3                      |
| 16:15 | Fyraften                           |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww08](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww08)

## Kommentarer

- ..
