---
Week: 12
Content: Virtuelt netværks lab
Material: See links in weekly plan
Initials: NISI
---

# Uge 12 - Virtuelt netværks lab

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Virtuelt lab opsat i vmware workstation

### Læringsmål

Den studerende har viden om og forståelse for:

- Sikkerhedsniveau i de mest anvendte netværksprotokoller

Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

- Opsætte et simpelt netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik

Den studerende kan:

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Afleveringer

- Ingen

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet                          |
| :---: | :--------------------------------- |
| 9:00  | IT Sikkerheds nyheder              |
| 9:15  | Præsentationer fra studerende K2   |
| 9:45  | Introduktion til dagens øvelser K1 |
| 10:00 | Øvelser K1/K4                      |
| 11:30 | Frokost                            |
| 12:30 | Studiestart prøve                  |
| 13:30 | Øvelser K2/K3                      |
| 16:15 | Fyraften                           |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12)

## Kommentarer

- Øvelserne i denne uge dækker også næste uge
