---
Week: 13
Content: Catch up / Selvstudie
Material: See links in weekly plan
Initials: NISI
---

# Uge 13 - Catch up / Selvstudie

## Mål for ugen

Herunder kan du læse ugens forskellige mål

Ugen er uden underviser og studerende skal arbejde videre med øvelser fra uge 12.

### Praktiske mål

- Arbejd med det virtuelle lab og især øvelse 8 - Damn Vulnerable Web Application (DVWA)
- Lav øvelser fra tidligere uger som du ikke har nået endnu

### Læringsmål

Den studerende har viden om og forståelse for:

- Sikkerhedsniveau i de mest anvendte netværksprotokoller

Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

- Opsætte et simpelt netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik

Den studerende kan:

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Afleveringer

- Ingen

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet     |
| :---: | :------------ |
| 9:00  | Øvelser K2/K3 |
| 11:30 | Frokost       |
| 12:15 | Øvelser K2/K3 |
| 16:15 | Fyraften      |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww12)

## Kommentarer

- ..
