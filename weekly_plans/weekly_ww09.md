---
Week: 09
Content: Bash scripting, programmering i Python med netværk
Material: See links in weekly plan
Initials: NISI
---

# Uge 09 Scripting i bash og Python med netværk

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Læringsmål opfyldt (øvelser lavet)

### Læringsmål

- Den studerende har viden om grundlæggende programmeringsprincipper
- Den studerende kan:
  - Konstruere simple programmer der kan bruge netværk
  - Læse andres bash scripts samt gennemskue og ændre i dem
  - Håndtere mindre bash scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
  - Konstruere og anvende tools med bash

## Afleveringer

- Ingen afleveringer, men det er altid en god ide at tage notater mens du arbejder!

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet                          |
| :---: | :--------------------------------- |
| 9:00  | IT Sikkerheds nyheder              |
| 9:15  | Præsentationer fra studerende K2   |
| 9:45  | Introduktion til dagens øvelser K1 |
| 10:00 | Øvelser K1/K4                      |
| 11:30 | Frokost                            |
| 12:15 | Øvelser K1/K4                      |
| 13:45 | Øvelser K2/K3                      |
| 16:15 | Fyraften                           |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww09](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww09)

## Kommentarer

- None
