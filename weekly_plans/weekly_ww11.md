---
Week: 11
Content: Ingen undervisning
Material: See links in weekly plan
Initials: NISI
---

# Uge 11

## Mål for ugen

Ingen undervisning

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww11](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww11)

## Kommentarer

- ..
