---
Week: 14
Content: Forberedelse til eksamen
Material: See links in weekly plan
Initials: NISI
---

# Uge 14 - Forberedelse til eksamen

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Alle studerende har en plan og et overblik over hvad der skal arbejdes med op til eksamen.
- Semester evaluering udfyldt.

### Læringsmål

- Den studerende kan reflektere over egen læring og skabe overblik over hvilken forberedelse der er nødvendig for at være velforberedt til eksamen.

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet                          |
| :---: | :--------------------------------- |
| 9:00  | IT Sikkerheds nyheder              |
| 9:15  | Præsentationer fra studerende K2   |
| 9:45  | Introduktion til dagens øvelser K1 |
| 10:00 | Øvelser K1/K4                      |
| 11:30 | Frokost                            |
| 12:15 | Øvelser K1/K4                      |
| 13:45 | Øvelser K2/K3                      |
| 16:15 | Fyraften                           |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww14](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww14)

## Kommentarer

- Semester evaluering er sendt til jeres studie mail
