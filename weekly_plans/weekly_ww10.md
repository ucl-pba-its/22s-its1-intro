---
Week: 10
Content: Scripting i Powershell
Material: See links in weekly plan
Initials: NISI
---

# Uge 10 - Scripting i Powershell (og lidt mere bash scripting) - Kladde/opdateres

## Mål for ugen

Herunder kan du læse ugens forskellige mål

Som forberedelse kig på eksempel scripts i Microsoft's dokumentation [https://docs.microsoft.com/en-us/powershell/scripting/samples/sample-scripts-for-administration?view=powershell-7.2](https://docs.microsoft.com/en-us/powershell/scripting/samples/sample-scripts-for-administration?view=powershell-7.2)  
Du må meget gerne kigge mere i dokumentationen hvis du har tid til det.

### Praktiske mål

- Øvelser gennemført og læringsmål opfyldt

### Læringsmål

Den studerende kan:

- Læse andres powershell scripts samt gennemskue og ændre i dem
- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Afleveringer

- Scripts dokumenteret i portfolio

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

Dagens skema er anderledes fordi jeres underviser skal ttil møde om formiddagen.  
Vi starter sammen kl. 9, resten af formiddagen arbejder i selvstændigt med opgave 1.
Efter frokost fortsætter vi sammen.

### Tirsdag

|  Tid  | Aktivitet                          |
| :---: | :--------------------------------- |
| 9:00  | IT Sikkerheds nyheder              |
| 9:15  | Præsentationer fra studerende K2   |
| 9:45  | Introduktion til dagens øvelser K1 |
| 10:00 | Øvelse 1 - K2/K3                   |
| 11:30 | Frokost                            |
| 12:15 | Øvelse 2 + 3 K1/K4                 |
| 15:30 | Øvelser K2/K3                      |
| 16:15 | Fyraften                           |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww10](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww10)

## Kommentarer

- ..
