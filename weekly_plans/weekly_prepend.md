---
title: '22S ITS1 Introduktion til IT Sikkerhed'
subtitle: 'Ugeplaner'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'Introduktion til IT Sikkerhed, ugeplaner'
---

# Introduktion

Dette dokument indeholder ugeplaner for faget Introduktion til IT Sikkerhed forår 2022.
Dokumentet er levende og opdateres løbende igennem semestret.

Hver ugeplan indeholder mål og program på ugentlig basis.
