---
Week: 06
Content: GIT, Gitlab, Jobs, Nyheder
Material: See links in weekly plan
Initials: NISI
---

# Uge 06 - Introduktion til faget

## Mål for ugen

Herunder kan du læse ugens forskellige mål

Forventningsafstemning, Kompentence afklaring, valg af præsentationsemne, Nyheds kanaler, Jobmuligheder/søgeagent

### Praktiske mål

- Forudsætnings survey udfyldt
- Hver studerende har skrevet sig på et præsentations emne
- Fælles dokument til deling af nyhedskanaler er oprettet og delt
- Alle studernede har oprettet en søgeagent på jobindex

### Læringsmål

- Den studerende kender fagets læringsmål og fagets semester indhold
- Den studerende ved hvor nyheder om it sikkerhed kan findes
- Den studerende kan vurdere egne faglige emner i relation til fagets læringsmål
- Den studerende har dannet sig et overblik over karriere muligheder

## Afleveringer

- Forudsætnings survey
- Præsentations emne
- Dokument til deling af nyhedskanaler

## Skema

Herunder er det planlagte program for ugen, det kan ændre sig baseret på input fra studerende mm.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 9:00  | Introduktion til dagen |
| 9:45  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K1/K4          |
| 13:45 | Øvelser K2/K3          |
| 16:15 | Fyraften               |

### Fredag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Fredags øvelser K2/K3  |
| 10:00 | Øvelser fra øvrige fag |

## Øvelser

Se denne uges øvelser: [https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww06](https://ucl-pba-its.gitlab.io/22s-its1-intro/exercises/exercises_ww06)

## Kommentarer

- ..
