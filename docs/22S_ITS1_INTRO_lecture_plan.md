---
title: '22S PBa IT sikkerhed'
subtitle: 'Fagplan for Introduktion til IT sikkerhed'
filename: '22S_ITS1_INTRO_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for Introduktion til IT sikkerhed
skip-toc: false
semester: 22S
---

# Introduktion

Formålet med faget er at få alle studerende op på et fælles grundniveau.
Elementet omhandler grundlæggende programmering og netværk med henblik på it-sikkerhed.  
Den studerende bliver introduceret til programmering med et programmeringssprog, der normalt anvendes indenfor sikkerhedsverdenen.  
Den studerende skal have viden, færdigheder og kompetencer i centrale begreber i forhold til it-sikkerhed, i forhold til netværk (grundlæggende begreber som trafik monitorering ved sniffing). Yderligere kigges der på sikkerhedsaspekter ved protokollerne.

De studerende vil have forskellige baggrund og dermed kompentencer når de starter. Dette vil vi bruge til at få de studerende til at hjælpe hinanden og på den blive skarpe på deres egne styrker og svagheder.

Faget er på 5 ECTS point.

# Læringsmål

Viden
Den studerende har viden om og forståelse for:

- Grundlæggende programmeringsprincipper
- Grundlæggende netværksprotokoller
- Sikkerhedsniveau i de mest anvendte netværksprotokoller

Færdigheder
Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

- Anvende primitive datatyper og abstrakte datatyper
- Konstruere simple programmer der bruge SQL databaser
- Konstruere simple programmer der kan bruge netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
- Opsætte et simpelt netværk.
- Mestre forskellige netværksanalyse tools
- Læse andres scripts samt gennemskue og ændre i dem

Kompetencer
Den studerende kan:

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

Se [Studieordningen sektion 2.1](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=77112065-b434-4e74-a5b8-336327053716)

# Lektionsplan (opdateres løbende igennem semestret)

| Indhold                                                     | Uge | Beskrivelse                                                                                                                                                                                                                                                                                                                                                                                  |
| :---------------------------------------------------------- | :-- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Introduktion til faget                                | 06  | Forventningsafstemning, Kompentence afklaring, valg af præsentationsemne, Nyheds kanaler, Jobmuligheder/søgeagent                                                                                                                                                                                                                                                                            |
| Værktøjer og portfolio | 07  | Git, ssh, gitlab, markdown og hjemmeside til portfolio                                                                                                                                                                                                                                                                      |
| Programmering i Python 1                              | 08  | Grundlæggende programmeringsprincipper med Socket, Urllib, HTML parsing. Anvende primitive datatyper og abstrakte datatyper, Konstruere simple programmer der kan bruge netværk                                                                                                                                                                                                              |
| Programmering med database og Scripting i bash         | 09  | Konstruere simple programmer der bruger SQL database, Grundlæggende programmeringsprincipper, Anvende primitive datatyper og abstrakte datatyper. Læse andres bash scripts samt gennemskue og ændre i dem, Håndtere mindre bash scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv, Konstruere og anvende tools med bash til f.eks. at opsnappe samt filtrere netværkstrafik |
| Scripting i Powershell (og lidt mere bash scripting)  | 10  | Læse andres powershell scripts samt gennemskue og ændre i dem, Håndtere mindre powershell scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv, Konstruere og anvende tools med powershell til f.eks. at opsnappe samt filtrere netværkstrafik                                                                                                                                 |
| Grundlæggende netværk                                  | 11  | ISO model, IP, ports, packages, wireshark, mirror ports, Opsætte et simpelt netværk, Mestre forskellige netværksanalyse tools                                                                                                                                                                                                                                                                |
| Virtuelt netværks lab 1                                       | 12  | network tools, sniffing scanning and such, Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik                                                                                                                                                                                                                                                                   |
| Virtuelt netværks lab 2                                   | 13  | vmware workstation, netværk, juniper, junos, kali Linux, udlevering af eksamensspørgsmål                                                                                                                                                                                                                                                                                                                                     |
| Repetition                                            | 14  | Eksamens Repetition og forberedelse                                                                                                                                                                                                                                                                                                                                                          |
| Eksamen                                               | 16  | Eksamen, tidsplan er på wiseflow.                                                                                                                                                                                                                                                                                                                                                  |

## Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt
