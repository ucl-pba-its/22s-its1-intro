---
title: '22S ITS1 INTRO eksamens spørgsmål'
subtitle: 'Eksamens spørgsmål'
filename: '22S_ITS1_INTRO_eksamens_opgave'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamens spørgsmål
skip-toc: false
semester: 22S
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _intro til it sikkerhed_.

# Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit 5.2.2_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern censur.

For hvert spørgsmål bør den studerende forberede en præsentation/oplæg på max 10 minutter.  
Efter den studerendes præsentation stiller eksaminator og censor spørgsmål.  
Oplæg og spørgsmål tager samlet 20 minutter, de sidste 5 minutter er afsat til votering.

# Eksamens spørgsmål

1. Netværk med fokus på OSI model og netværksprotokoller samt sikkerhedsniveau
2. Programmering i Python med database og fokus på database sikkerhed.
3. Scripting i bash og powershell
4. Programmering i Python med sockets og fokus på sikkerhed i protokoller.

# Eksamens datoer

- Forsøg 1 - 2022-04-19
- Forsøg 2 - 2022-08-10
- Forsøg 3 - 2022-09-06
