# Læringsmål uge 6

- Den studerende kender fagets læringsmål og fagets semester indhold
- Den studerende ved hvor nyheder om it sikkerhed kan findes
- Den studerende kan vurdere egne faglige emner i relation til fagets læringsmål
- Den studerende har dannet sig et overblik over karriere muligheder

# Øvelser uge 6

- Øvelse 1 - Kompetence afklaring
- Øvelse 2 - Markdown
- Øvelse 3 - Studieordning og læringsmål
- Øvelse 4 - Nyhedskanaler
- Øvelse 5 - Jobmuligheder
- Øvelse 6 - Præsentationsplan

# Læringsmål uge 7

- Den studerende har viden om grundlæggende programerings principper
- Den studerende kan læse andres scripts samt gennemskue og ændre i dem

# Øvelser uge 7

- Øvelse 1 - Git/gitlab og ssh nøgler
- Øvelse 2 - gitlab pages til personlig portfolio
- Øvelse 3 - gitlab pages til it-sikkerheds nyhedskilder
- Øvelse 4 - refleksions spørgsmål

Spørgsmål:

- Hvad kan du fortælle om ssh nøgler ?
- Hvorfor er det ikke sikkert at gemme sine private nøgler på andre computere? Hvad risikerer du ved det?
- Er det sikkert at forwarde din private ssh nøgle med en user agent ? Uddyb dit svar.
- Hvordan fungerer pipelines og yaml, bare et overblik 

# Læringsmål uge 8

- Den studerende har viden om grundlæggende programmeringsprincipper
- Den studerende kan:
  - Anvende primitive datatyper og abstrakte datatyper
  - Konstruere simple programmer der bruge SQL databaser
  - Konstruere simple programmer der kan bruge netværk

# Øvelser uge 8

- Øvelse 1 - Python recap
- Øvelse 2 - Programmering med database

# Læringsmål uge 9

- Den studerende har viden om grundlæggende programmeringsprincipper
- Den studerende kan:
  - Konstruere simple programmer der kan bruge netværk
  - Læse andres bash scripts samt gennemskue og ændre i dem
  - Håndtere mindre bash scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
  - Konstruere og anvende tools med bash

# Øvelser uge 9

- Øvelse 1 - bash basics
- Øvelse 2 - Forstå bash script + tilrette script
- Øvelse 3 - Programmering med netværk

# Læringsmål uge 10

Den studerende kan:

- Læse andres powershell scripts samt gennemskue og ændre i dem
- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

# Øvelser uge 10

- Øvelse 1 - Bash script til vm konfigurering
- Øvelse 2 - Powershell basics
- Øvelse 3 - Powershell script

# Læringsmål uge 12/13

Den studerende har viden om og forståelse for:

- Sikkerhedsniveau i de mest anvendte netværksprotokoller

Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

- Opsætte et simpelt netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik

Den studerende kan:

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

# Øvelser uge 12/13

- Øvelse 1 - Opsætning af virtuelle maskiner i vmware
- Øvelse 2 - Opsætning af vsrx virtuel router
- Øvelse 3 - Seriel forbindelse til vsrx virtuel router
- Øvelse 4 - konfigurer vsrx password og vsrx navn
- Øvelse 5 - konfigurer vsrx routing
- Øvelse 6 - tilføj interface til vsrx
- Øvelse 7 - Kali linux vm på netværket
- Øvelse 8 - Damn Vulnerable Web Application (DVWA)

# Læringsmål uge 14

- Den studerende kan reflektere over egen læring og skabe overblik over hvilken forberedelse der er nødvendig for at være velforberedt til eksamen.

# Øvelser uge 14
