# Vars til farver
# Check user input (IP addr, if/else)
# user input
$IP = $args[0]

# check if user supplied argument
if ($IP.Count -eq 1) {
    Write-Host "target: " -NoNewline 
    Write-Host $IP -ForegroundColor RED 
}
else {
    Write-Host "Usage example: ./nmapScans.sh 10.10.10.121"
    exit 1
}

$DATE = Get-Date -Format "yyyyMMdd"
$FILENAME = "quick_nmapscan_$IP`_$DATE.log"
Write-Host $FILENAME

nmap -T4 -F $IP > $FILENAME
