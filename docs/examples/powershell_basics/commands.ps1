## Show powershell version

$PSVersionTable 
$PSVersionTable.PSVersion



#Get-Verb # get list of approved verbs
#Get-Command # list all available cmdlets
Get-Help # Invoke built in help system
# Get-Help Get-Member # 
# Update-Help # update help files, need admin mode

# Get-Command File* # find anything related to files.

# Get-Process # all running processes on machine

# Get-Process | Get-Member # get process types

