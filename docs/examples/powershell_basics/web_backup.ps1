# set default params
Param(
  [string]$Path = './app',
  [string]$DestinationPath = './',
  [switch]$PathIsWebApp 
)

# Error handling, check if extensions are web related
If ($PathIsWebApp -eq $True) {
   Try 
   {
     $ContainsApplicationFiles = "$((Get-ChildItem $Path).Extension | Sort-Object -Unique)" -match  '\.js|\.html|\.css'

     If ( -Not $ContainsApplicationFiles) {
       Throw "Not a web app"
     } Else {
       Write-Host "Source files look good, continuing"
     }
   } Catch {
    Throw "No backup created due to: $($_.Exception.Message)"
   }
}

# Check if path exits
If (-Not (Test-Path $Path)) 
{
  Throw "The source directory $Path does not exist, please specify an existing directory"
}

# get date and create file name parameter
$date = Get-Date -format "yyyy-MM-dd"
$DestinationFile = "$($DestinationPath + 'backup-' + $date + '.zip')"

#check if file exists
If (-Not (Test-Path $DestinationFile)) 
{
  Compress-Archive -Path $Path -CompressionLevel 'Fastest' -DestinationPath "$($DestinationPath + 'backup-' + $date)"
  Write-Host "Created backup at $($DestinationPath + 'backup-' + $date + '.zip')"
} Else {
  Write-Error "Today's backup already exists"
}