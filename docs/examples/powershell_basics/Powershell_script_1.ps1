# Powershell_script.ps1

$PI = 3.14
Write-Host "The value of `$PI is $PI"

Get-ExecutionPolicy

Write-Host 'Here is $PI' # Prints Here is $PI
Write-Host "An expression $($PI + 1)" # Prints An expression 4.14

# Create new profile for current user, only worked on azure ?

New-Item `
  -ItemType "file" `
  -Value 'Write-Host "Hello nisi, welcome back" -foregroundcolor Green ' `
  -Path $Profile.CurrentUserCurrentHost -Force