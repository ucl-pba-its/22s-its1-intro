#!/bin/bash

# log stuff throughout with tee
LOG="/home/nisi/scripts/deploy.log"

# sleep until networkmanager reports connected
until nmcli general | grep "^connected" >/dev/null; do
        sleep 2
done

# when connected
# get number of updates available
# https://askubuntu.com/questions/269606/apt-get-count-the-number-of-updates-available
PACKAGES=LANG=C apt-get upgrade -s |grep -P '^\d+ upgraded'|cut -d" " -f1
# install if packages available
if [[ "$PACKAGES" -ne 0 ]]; then

        echo -e  "\nINFO: New packages found \n" | tee $LOG
        apt-get update -y -q | tee -a $LOG
        apt-get upgrade -y -q | tee -a $LOG # upgrade silent

else
        echo -e "\nINFO: No updatable packages available" | tee $LOG
fi

# check if packages is installed
# https://stackoverflow.com/questions/1298066/how-can-i-check-if-a-package-is-installed-and-install-it-if-not
pkgs='apache2'
install=false
for pkg in $pkgs; do
        status="$(dpkg-query -W --showformat='${db:Status-Status}' "$pkg" 2>&1)"
        if [ ! $? = 0 ] || [ ! "$status" = installed ]; then
                install=true
                break
        else
                echo -e "\n$pkg already installed" | tee -a $LOG
        fi
done
if "$install"; then
  sudo apt-get install -y -q $pkgs | tee -a $LOG
fi

#pkg=apache2
#status="$(dpkg-query -W --showformat='${db:Status-Status}' "$pkg" 2>&1)"
#if [ ! $? = 0 ] || [ ! "$status" = installed ]; then 
#       echo -e "\nINFO: Apache2 not installed\n"
#       apt-get install -y -q apache2 | tee -a $LOG #install apache2

#else

#       echo -e "\napache2 already installed" | tee -a $LOG
#fi

exit 0
