#!/bin/bash

#Add color to the shell output https://techstop.github.io/bash-script-colors/
RED='\033[0;31m'
WHITE='\033[0;37m'
NC='\033[0m' # no color

# help the user
echo -e "${RED}RUN THIS SCRIPT AS SUDO USER${WHITE}"

# Check if args array has 1 element
if [ $# -eq 1 ]; then
        echo -e "Target ${RED}IP:$1${WHITE}"
# if args length is wrong help the user
else
        echo "Your command line is incorrect"
        echo -e "Usage example: ${RED} sudo ./nmapScans.sh 10.10.10.121 ${WHITE}"
        exit 1 # exit with error code 1
fi # end if statement

#Get the date in supplied format, will be used in filename
DATE=`date +%Y-%m-%d`
filename=$1_$DATE # concatenate supplied arg with date

echo -e "Running a ${RED} quick scan ${WHITE}..." # info to user
nmap -T4 -F $1 > quick_nmapscan_$filename.txt # run nmap on the specified ip
echo -e "Results -> ${RED}quick_nmapscan_$filename.txt${WHITE}" # tell the user where to find results file

exit 0 # exit script without errors 


