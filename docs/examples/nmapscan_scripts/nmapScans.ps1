# Check if args array has 1 element
if ($args.Length -eq 1) {
    Write-Host "Target " -ForegroundColor Red -NoNewline; Write-Host "IP: $($args[0])"
}
# if args length is wrong help the user
else {
    Write-Host "Your command line is incorrect" -ForegroundColor RED
    Write-Host "Usage example: " -ForegroundColor Red -NoNewline; Write-Host ".\nmapScans.sh 10.10.10.121" 
    exit 1 # exit with error code 1
}

$DATE = Get-Date -Format "yyyy/MM/dd" # create a date-time string using formatting
$filename="quick_nmapscan_$($args[0])_$DATE.txt" # create a filename based on supplied arg

$results = nmap -T4 -F $args[0] # run nmap on the specified ip, save result in variable
$results > $filename # save results in file 
Write-Host 'Results ->' $filename # tell the user where to find results file

Write-Output $results # write results to console, using write-output because it shows line breaks.

exit 0 # exit script without errors 